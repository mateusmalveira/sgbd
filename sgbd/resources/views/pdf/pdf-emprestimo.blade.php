<!DOCTYPE html>
<html lang="pt" xmlns="http://www.w3.org/1999/html">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">    
    <!-- Bootstrap core CSS -->
    <style>
      body{
        font-family: 'Roboto', sans-serif !important;
        background-color:#ffff8d;
      }
      p,h6{
        text-align: center;
        margin-top:0px;
        padding-top:0px;
      }
      td{
        font-size: 0.4em;
        }
      .title{
        padding-left:0.9em;
        font-weight: bold;
      }  
      table{
        margin: 0px auto;
      }
      .im{
        margin-top:1.5em !important;
        margin-bottom:0px;
        padding-bottom: 0px;
      }
      </style>
</head>
<body>
  <div class="">
    <div class="row">      
        <div class="mr-auto"> 
        <h6 class="im"><img src="./img/opt/icon.png" width="10%"></h6>        
        <h6>{{ config('app.name')}}</h6>
        <p>Recibo de Empréstimo</p>
      </div>    
    </div>
    <table>      
      <tbody>
        <tr><td class="title">Nome do {{$data->Tipo}}:</td><td>{{ $data->Nome }}</td></tr>
        <tr><td class="title">Título:</td><td>{{ $data->titulo_livro }}</td></tr>
        <tr><td class="title">Data de Empréstimo:</td><td>{{ date('d-m-Y',strtotime($data->data_emprestimo))}}</td></tr>
        <tr><td class="title">Data de Devolução:</td><td> {{ date('d-m-Y',strtotime($data->data_devolucao)) }}</td></tr>
      </tbody>
    </table> 
  </div>
</body>
</html>