@extends ('layouts.admin')
@section ('conteudo')
@include('layouts.errors')

<div class="card">
  <div class="card-header">
      <!--Name Field -->
      <i class="fas fa-pencil-alt "></i>Detalhes Aluno:  {{ $user->name }}
  </div>
  <div class="card-body">    
      <div class="form-row">
          <div class="form-group col-sm-3">
          <label for="name" >{{ __('Nome Completo') }}</label>                            
              <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$user->name}}" required>
          </div>   
          <div class="form-group col-sm-3">
              <label for="cpf">{{ __('CPF (Somente Números)') }}</label>                               
                  <input id="cpf" type="text" class="form-control{{ $errors->has('cpf') ? ' is-invalid' : '' }}" name="cpf" value="{{$user->cpf}}" required >
      
          </div>
          <div class="form-group col-sm-2">
              <label for="cep" >{{ __('CEP') }}</label>
              
                  <input id="cep" type="text" class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }}" name="cep" value="{{$user->cep}}" required >
      
          </div>
          <div class="form-group col-sm-4">
              <label for="logradouro" >{{ __('Endereço') }}</label>
              
                  <input id="logradouro" type="text" class="form-control{{ $errors->has('logradouro') ? ' is-invalid' : '' }}" name="logradouro" value="{{$user->endereço}}" required >
          </div>
          <div class="form-group col-sm-3">
              <label for="bairro">{{ __('Bairro') }}</label>                               
                  <input id="bairro" type="text" class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }}" name="bairro" value="{{$user->bairro}}" required >
          </div>
          <div class="form-group col-sm-3">
              <label for="cidade" >{{ __('Cidade') }}</label>                               
                  <input id="cidade" type="text" class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }}" name="cidade" value="{{$user->cidade}}" required >
          </div>
          <div class="form-group col-sm-3">
              <label for="estado">{{ __('Estado') }}</label>                               
                  <input id="estado" type="text" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado" value="{{$user->estado}}" required >
          </div>
          <div class="form-group col-sm-3">
              <label for="telefone" >{{ __('Telefone') }}</label>                               
                  <input id="telefone" type="text" class="phone form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" name="telefone" value="{{$user->telefone}}" required >
      
          </div>
          <div class="form-group col-sm-3">
              <label for="email" >{{ __('E-Mail Address') }}</label>
              
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$user->email}}" required>
          </div>
          <div class="form-group col-sm-3">
              <label for="username">{{ __('Username') }}</label>    
              
                  <input max="20" id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{$user->username}}" required>
          </div>
          
          <div class="form-group col-sm-3">
              <label for="matricula">{{ __('Matricula:') }}</label>
                  <input data-mask="000000" data-mask-selectonfocus="true" id="matricula" type="text" class=" matricula form-control{{ $errors->has('matricula') ? ' is-invalid' : '' }}" mask='000000' name="matricula" value="{{$aluno->matricula_aluno}}" required>  
  
          </div>
              <div class="form-group col-sm-3">
                  <label for="curso">Curso: </label><br>
                                    <select id='curso'   data-live-search="true" class="selectpicker" name='curso' style="width:100%">
                      <option selected value='{{$cursoal->cod_curso}}'>{{$cursoal->nome_curso}} </option>
                      @foreach($cursos as $c)
                      <option value="{{$c->cod_curso}}">{{$c->nome_curso}}</option>
                      @endforeach          
                  </select>
      
              </div>
              <div class="form-group col-sm-3">
                <label for='data'>Data de Matricula</label>
                  <div class='input-group date' id='datetimepicker'>
                      <input id='data' name='data' type='text' class=" data form-control" value="{{date('dmY',strtotime($aluno->data_ingreso))}}" />
                      <span class="input-group-addon">
                          <span class="fas fa-calendar">
                          </span>
                      </span>
                  </div>
              </div>
          </div>
  </div>
</div> 
<script>  
$(":input").attr('disabled', true); 
</script> 

@endsection