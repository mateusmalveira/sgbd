@extends ('layouts.admin')
@section ('conteudo')

    <!-- Errors -->
    @include ('layouts.errors')    

    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Editar Administrador:  {{ $administradores->name }}
        </div>
        <div class="card-body">
            {!! Form::model($administradores, ['method'=>'PATCH', 'route'=>['administradores.update',$administradores->id]]) !!}
            {{ Form::token() }}
            <div class='form-row'>
                <div class="form-group col-sm-3">
                    <label for="nome">Nome: </label>
                <input type="text" value="{{ $administradores->name }}" name="nome" id="nome" class="form-control">
                  </div>
                  <div class="form-group col-sm-6">
                      <label for="email">Email: </label>
                  <input type="email" value="{{ $administradores->email }}" name="email" id="email" class="form-control">
                  </div>
                  <div class="form-group col-sm-3">
                      <label for="username">Username: </label>
                  <input type="text" value="{{ $administradores->username }}" name="username" id="username" class="form-control">
                    </div>  
                    <div class="form-group col-sm-3">
                        <label for="username">Password: </label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                      </div>
                      <div class="form-group col-sm-3">
                          <label for="username">Confirmar Password: </label> 
                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                        </div>                 
              </div>
            <!--.. Aqui-->
            <!--buttons-->
            @include ('layouts.update-buttom')
            {!! Form::close() !!}
        </div>
    </div>

@endsection