<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<!--head-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>{{ config('app.name', 'Sistema de Gerenciamento de Bibliotecas') }}</title>
    <link rel="shortcut icon" href="{{asset('img/opt/icon.svg')}}" type="img/svg">

</head>
<body>
<!--Navbar 01 -->
<nav class="navbar navbar-expand-lg navbar-light white home-page" id="navbar">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img class="icon-color" src="{{asset('img/opt/icon.svg')}}" title="Icon" class="d-inline-block" width="40em" alt="">
            <h1 class="d-inline-block">
                {{ config('app.name') }}</h1>
            <br>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse text-uppercase" id="navbarNavDropdown">
            <div class="ml-auto">
                <ul id="menu-menu-tags" class="nav navbar-nav"> 
                    <li class="nav-item dropdown 
                    @if(Route::current()->getName() == 'editprofile.editAluno') active
                    @elseif(Route::current()->getName() == 'editprofile.editProfessor') active
                    @elseif(Route::current()->getName() == 'editprofile.editFuncionario') active
                    @elseif(Route::current()->getName() == 'editprofile.editBiblio') active
                    @endif                        
                    ">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class='fas fa-cog'></i>Opções</a>
                                    
                            </a>

                            {{--Regras para editar perfil seguindo alth--}}
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            @if(Auth::guard('web')->check()) 
                                {{-- if by type user--}}
                                <a class="dropdown-item" href=
                                @if(Auth::user()->tipo_usuario == 'aluno')
                                "{{route('editprofile.editAluno', ['id' => Auth::user()->id])}}"
                                @elseif(Auth::user()->tipo_usuario == 'professor')"{{route('editprofile.editProfessor', ['id' => Auth::user()->id])}}" 
                                @elseif(Auth::user()->tipo_usuario == 'funcionario')"{{route('editprofile.editFuncionario', ['id' => Auth::user()->id])}}" 
                                @endif
                                >Editar Perfil
                                 </a>
                            </div>
                            @elseif(Auth::guard('admin')->check())
                            <a class="dropdown-item" href="{{route('administradores.edit', ['id' => Auth::user()->id])}}"
                            >Editar Perfil
                            </a>

                            @elseif(Auth::guard('biblio')->check())
                            <a class="dropdown-item" href="{{route('editprofile.editBiblio', ['id' => Auth::user()->id])}}"
                            >Editar Perfil
                            </a>

                            @endif



                           
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{Auth::user()->name}}
                                
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                 {{ __('Sair') }}
                             </a>
 
                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                 @csrf
                             </form>
                        </div>
                    </li>

                </ul>
            </div>
        </div>

    </div>
</nav>
<!-- Navbar 02-->
<nav class="navbar navbar-expand-lg navbar-light white" id='navbar02'>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown02" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse text-uppercase" id="navbarNavDropdown02">
        <div class="mx-auto">
            <ul id="menu-menu-tags" class="nav navbar-nav">
                <li class="nav-item 
                @if(Route::current()->getName() == 'admin.dashboard') active
                @elseif(Route::current()->getName() == 'home') active
                @elseif(Route::current()->getName() == 'biblio.dashboard') active
                @endif" id="menu-item-301">
                <a class="nav-link" href="@if(Auth::guard('admin')->check()) 
                    {{route('admin.dashboard')}}
                    @elseif(Auth::guard('biblio')->check())
                    {{route('biblio.dashboard')}}
                    @else
                    /home
                    @endif">
                        <i class='fas fa-home'></i>
                        <h6>Início</h6>
                    </a>
                </li>
                <li class="nav-item dropdown
                    {{-- Active Menu --}}
                    @if(Route::current()->getName() == 'livros.index') active
                    @elseif(Route::current()->getName() == 'livros.create') active
                    @elseif(Route::current()->getName() == 'livros.edit') active
                    @elseif(Route::current()->getName() == 'categorias.index') active
                    @elseif(Route::current()->getName() == 'categorias.create') active
                    @elseif(Route::current()->getName() == 'categorias.edit') active
                    @elseif(Route::current()->getName() == 'autores.index') active
                    @elseif(Route::current()->getName() == 'autores.create') active
                    @elseif(Route::current()->getName() == 'autores.edit') active
                    @elseif(Route::current()->getName() == 'cursos.index') active
                    @elseif(Route::current()->getName() == 'cursos.create') active
                    @elseif(Route::current()->getName() == 'cursos.edit') active                   
                    @elseif(Route::current()->getName() == 'professoresadmin.index') active
                    @elseif(Route::current()->getName() == 'professoresadmin.create') active
                    @elseif(Route::current()->getName() == 'professoresadmin.edit') active
                    @elseif(Route::current()->getName() == 'professoresadmin.details') active
                    @elseif(Route::current()->getName() == 'professoresbiblio.index') active
                    @elseif(Route::current()->getName() == 'professoresbiblio.create') active
                    @elseif(Route::current()->getName() == 'professoresbiblio.edit') active
                    @elseif(Route::current()->getName() == 'professoresbiblio.details') active
                    @elseif(Route::current()->getName() == 'alunosadmin.index') active
                    @elseif(Route::current()->getName() == 'alunosadmin.edit') active
                    @elseif(Route::current()->getName() == 'alunosadmin.details') active
                    @elseif(Route::current()->getName() == 'alunosbiblio.index') active
                    @elseif(Route::current()->getName() == 'alunosbiblio.edit') active
                    @elseif(Route::current()->getName() == 'alunosbiblio.details') active
                    @elseif(Route::current()->getName() == 'funcionariosadmin.index') active  
                    @elseif(Route::current()->getName() == 'funcionariosadmin.edit') active  
                    @elseif(Route::current()->getName() == 'funcionariosadmin.details') active    
                    @elseif(Route::current()->getName() == 'funcionariosbiblio.index') active  
                    @elseif(Route::current()->getName() == 'funcionariosbiblio.edit') active  
                    @elseif(Route::current()->getName() == 'funcionariosbiblio.details') active 
                    @elseif(Route::current()->getName() == 'bibliotecarios.index') active 
                    @elseif(Route::current()->getName() == 'bibliotecarios.edit') active 
                    @elseif(Route::current()->getName() == 'administradores.index') active  
                    @elseif(Route::current()->getName() == 'administradores.edit') active     
                    @endif
                ">
                    <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class='fas fa-file'></i>
                        <h6>Consultar @if(Auth::guard('admin')->check()) e Cadastrar @endif</h6>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        @if(Auth::guard('admin')->check())
                        <a class="dropdown-item" href="{{route('livrosadmin.index')}}">Livros</a>                       
                        <a class="dropdown-item  dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Usuarios </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="{{route('alunosadmin.index')}}">Alunos</a></li>
                                <li><a class="dropdown-item" href="{{route('professoresadmin.index')}}">Professores</a></li> 
                                <li><a class="dropdown-item" href="{{route('funcionariosadmin.index')}}">Funcionarios</a></li> 
                                </li> 
                                <li><a class="dropdown-item" href="{{route('bibliotecarios.index')}}">Bibliotecarios</a></li> 
                                <li><a class="dropdown-item" href="{{route('administradores.index')}}">Administradores</a></li> 
                            </li>                                 
                            </ul>                        
                        <a class="dropdown-item" href="/admin/emprestimos/">Emprestimos</a>
                        <a class="dropdown-item" href="/admin/autores">Autores</a>
                        <a class="dropdown-item" href="/admin/categorias">Categorias de Livros</a>
                        <a class="dropdown-item" href="/admin/cursos">Cursos</a>
                        @elseif(Auth::guard('biblio')->check())
                        <a class="dropdown-item " href="{{route('livrosbiblio.index')}}">Livros</a>
                        <a class="dropdown-item" href="/biblio/categorias">Categorias</a>
                        <a class="dropdown-item  dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Usuarios </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="{{route('alunosbiblio.index')}}">Alunos</a></li>
                                <li><a class="dropdown-item" href="{{route('professoresbiblio.index')}}">Professores</a></li> 
                                <li><a class="dropdown-item" href="{{route('funcionariosbiblio.index')}}">Funcionarios</a></li> 
                                </li>             
                            </ul>
                        <a class="dropdown-item" href="{{route('autoresbiblio.index')}}">Autores</a>
                        <a class="dropdown-item" href="/biblio/emprestimos">Emprestimos</a>
                        @else
                        <a class="dropdown-item " href="/livros">Livros</a>
                        <a class="dropdown-item" href="/autores">Autores</a>
                        <a class="dropdown-item" href="/categorias">Categorias</a>
                        @endif
                    </div>
                </li>
                @if(Auth::guard('admin')->check())
                <li class="nav-item" id="menu-item-301"> 
                    <a class="nav-link" href="/admin/emprestimos/create">
                        <i class="fas fa-book"></i>
                        <h6> Realizar Emprestimos </h6>
                    </a>
                </li>
                
                @endif
                @if(Auth::guard('biblio')->check())
                <li class="nav-item" id="menu-item-301"> 
                    <a class="nav-link" href="/biblio/emprestimos/create">
                        <i class="fas fa-book"></i>
                        <h6> Realizar Emprestimos </h6>
                    </a>
                </li>                  
                @endif
            </ul>
        </div>
    </div>
</nav>
<script type="text/javascript" src="{{asset('js/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>

<section class="content">
    <div class="container-inner">
@yield ('conteudo')
    </div>
</section>
{{--Tirar Depois--}}
<script type="text/javascript" src="{{asset('js/main/mask-custom.js')}}"></script>
</body>
</html>
