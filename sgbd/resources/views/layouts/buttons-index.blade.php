<a href="{{ URL::action($routeedit, $ids) }}"><button class="btn btn-sm btn-color">
    <i class="fas fa-edit"></i>
 </button></a>
 
 <a href="#"><button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#{{'delete'.$ids}}">
    <i class="fas fa-trash"></i>
 </button></a>