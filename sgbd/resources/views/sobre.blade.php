<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('img/opt/icon.svg')}}" type="img/svg">

    <title>{{ config('app.name') }}</title> 
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">   
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body class="home-page">

    <!-- Nav Bar Simplificada -->

    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="#">
            <img class="icon-color" width="9%" src="{{asset('img/opt/icon.svg')}}" title="Icon" class="d-inline-block" alt="">
            <p class="d-inline-block">
                {{ config('app.name', 'Sistema de Gerenciamento de Bibliotecas') }}</h1>
            <p>
        </a>          
        @if (Route::has('login'))
        <div class="top-right links">           
            @auth('admin')
                <a href="{{ url('/admin') }}">Painel</a>                          
            @endauth
            @auth('web')
            <a href="{{ url('/home') }}">Home</a>
            @endauth   
        </div>
        @endif
        @guest
        <div class="top-right links">
        <a href="{{ route('login') }}">Login</a>
        <a href="{{ route('register') }}">Register</a>   
        </div>
        @endguest
    
      </div>
    </nav>

    <!--
       Renomeei a classe Billboard para Masthead, segundo template no W3Schools
       Adicionando o Formato responsivo.
     -->

    <header class="masthead text-white text-center" style="background: url('{{asset('img/opt/equipe.jpg')}}') no-repeat center center; background-size:auto 100%;">
      <div class=""></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h4 class="mt-5 mb-2">Trabalho feito para a disciplina de Tecnologias Web</h4>
            <h5 class="">Equipe das Delícias!!</h5>
            <h6 class=""><a href="https://mateusmalveira.me" target="_blank" class="text-white">Mateus Malveira</a>, <a href="https://github.com/rafaeljnasc" target="_blank" class="text-white"> Rafael José </a>, <a href="https://wesley.info" target="_blank" class="text-white"> Wesley Melo</a>, Barbara Cavalcante e <a href="https://lucgbrl.com" target="_blank" class="text-white"> Lucas Gabriel</a></h6>
          </div>          
        </div>
      </div>
    </header>    

    <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">              
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="/sobre">Sobre</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Política de Privacidade</a>
              </li>
              <li class="list-inline-item">
              <a href="{{route('admin.login')}}">Administrador</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{route('biblio.login')}}">Bibliotecario</a>
                      </li>
            </ul>
          <p class="text-muted small mb-4 mb-lg-0">&copy; {{date('Y')}}- {{config('app.name')}}. Todos os direitos reservados. <img width="2%" class="" src="{{asset('/img/opt/juice.png')}}"></p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fab fa-facebook-f fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fab fa-twitter fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <script type="text/javascript" src="{{asset('js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>

  </body>

</html>
