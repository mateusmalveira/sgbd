@extends ('layouts.admin')
@section ('conteudo')

@include('layouts.errors')

   <div class="card">
    <div class="card-header">

      <!-- Search Form -->
      <div class="form-row">
        <div class="col-md-4 py-1">
          <!-- name field -->
          <i class="fas fa-list"></i>Empréstimos
        </div>
        <div class="col-md-4 ">
            @if(Auth::guard('admin')->check())
            <div class="text-center">
                <a href="/admin/emprestimos/create"><button class="btn btn-outline-color ">Incluir</button></a>
            </div>
            @endif             
        </div>
        <div class="col-md-4">
          @if(Auth::guard('admin')->check())
            @include ('layouts.search',['search'=>'/admin/emprestimos'])
          @elseif(Auth::guard('biblio')->check())  
          @include ('layouts.search',['search'=>'/biblio/emprestimos'])
          @endif
        </div>
      </div>

    </div>
    <div class="card-body wsc">
      <!-- Table-->
      <table class="table table-hover table-responsive">
        <thead>
          <tr>
            <!-- Modificar o cabeçalho de acordo com a tela-->
            <th>#</th>            
            <th>ISBN</th>
            <th>Titulo</th>            
            <th>Nome</th>            
            <th>Data de Emprestimo</th>            
            <th>Data de Devolução</th>            
            <th>Ações</th>            
          </tr>
        </thead>
        <tbody>
            <!-- Table Body - só basta colocar 1 campo como exemplo-->
            @foreach ($emprestimo as $key=>$e)
            <tr>
               <td>{{ ++$key }}</td>               
               <td>{{ $e->ISBN }}</td>
               <td>{{ $e->titulo_livro }}</td>               
               <td>{{ $e->Nome }}</td>               
               <td>{{ $e->data_emprestimo }}</td>               
               <td>{{ $e->data_devolucao }}</td>               
               <td>
                 
               
               <a title='Imprimir Recibo'  href="{{ URL::action('EmprestimoController@generatepdf', $e->EN) }}" class="btn btn-sm btn-color text-white"><i class="fas fa-file-pdf"></i></a>
               <a href="#"><button class=" text-white btn btn-sm btn-warning" data-toggle="modal" title='Devolver Livro' data-target="#{{'devolve'.$e->NE}}">
                  <i class="fas fa-undo-alt"></i>
               </button></a>             
               </td>
               
            </tr>
               @endforeach
            </tbody>
         </table>
         @if(Auth::guard('admin')->check())
           <?php $acao = 'emprestimosadmin.destroy'; ?>

          @foreach ($emprestimo as $key=>$e)          
          @include('layouts.modals.modal-devolve',[
              
            'nametype'=>'Devolver Livro: ',
            'ids'=>$e->NE,
            'namefield'=>$e->titulo_livro,  
            'buttonname'=>'Devolver',            
            'action'=>"$acao",  
            'useral'=>$e->Nome,
            'userty'=>$e->Tipo,
            'dateep' =>$e->data_emprestimo,
            'datedv' =>$e->data_devolucao,
            'multa' => '0.30'
          ])
          @endforeach 
         @elseif(Auth::guard('biblio')->check())
         <?php $acao = 'emprestimosbiblio.destroy'; ?>
            @foreach ($emprestimo as $key=>$e)
            @include('layouts.modals.modal-devolve',[
                
              'nametype'=>'Devolver Livro: ',
              'ids'=>$e->NE,
              'namefield'=>$e->titulo_livro,  
              'buttonname'=>'Devolver',            
              'action'=>"$acao",  
              'useral'=>$e->Nome,
              'userty'=>$e->Tipo,
              'dateep' =>$e->data_emprestimo,
              'datedv' =>$e->data_devolucao,
              'multa' => '0.30'
            ])
            @endforeach 
         @endif           
      </div>

   </div>
   <!-- Paginação -->
   {{$emprestimo->links('layouts.pagination')}}

@endsection