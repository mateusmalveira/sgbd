@extends ('layouts.admin')
@section ('conteudo')

    @include('layouts.errors')

    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Cadastrar Livros
        </div>
        <div class="card-body">
                        {!! Form::open(array('url'=>'/admin/livros','method'=>'POST', 'autocomplete'=>'off')) !!}
                        {{ Form::token() }}
                        <div class='form-row'>
                            <div class="form-group col-sm-3">
                              <label for="ISBN">ISBN: </label>
                              <input type="text" max="13" min="13" name="ISBN" id="ISBN" class="form-control">
                            </div>
                            <div class="form-group col-sm-9">
                              <label for="titulo">Título: </label>
                              <input type="text" name="titulo" id="titulo" class="form-control">
                            </div>                          
                            <div class="form-group col-sm-2">
                              <label for="lançamento">Ano de lançamento: </label>
                              <input type="number" min="1000" name="lancamento" id="lançamento" class="form-control">
                            </div>
                            <div class="form-group col-sm-2">
                              <label for="editora">Editora: </label>
                              <input type="text" name="editora" id="editora" class="form-control">
                            </div>
                            <div class="form-group col-sm-2">
                                <label for="copias">Categoria: </label><br>
                                <select id='' data-live-search="true" class="custom-select" name='categorias' style="width:100%">
                                    <option selected disabled>Selecione a Categoria</option>
                                    @foreach($categoria as $ct)    
                                <option value="{{$ct->id_categoria}}">{{$ct->nome}}</option>
                                    @endforeach
                                  </select>   
                            </div>
                            <div class="form-group col-sm-4">
                                    <label for="autores">Autores: </label><br>
                                    <select id='autores' multiple   data-live-search="true" class="selectpicker" name='autores[]' style="width:100%">
                                        <option disabled>Selecione o/a(s) Autor(es/as)</option>
                                        @foreach($autores as $a)    
                                    <option value="{{$a->cpf}}">{{$a->nome}}</option>
                                        @endforeach
                                      </select>   
                                </div>
                            <div class="form-group col-sm-2">
                              <label for="copias">Total de cópias: </label>
                              <input type="number" name="copias" id="copias" class="form-control">
                            </div>                           
                          </div>
                          
                          <!--.. Aqui-->
                          <!--buttons-->
                          @include ('layouts.register-buttons')
                       {!! Form::close() !!}
        </div>
    </div>    
    

@endsection