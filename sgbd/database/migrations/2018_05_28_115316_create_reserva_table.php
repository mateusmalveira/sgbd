<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserva', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ISBN',45);    
            $table->integer('id_user')->unsigned();                   
            $table->timestamp('data_reserva')->nullable(false)->useCurrent();
            $table->dateTime('data_final_reserva')->nullable(true);
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ISBN')->references('ISBN')->on('livro')->onDelete('cascade')->onUpdate('cascade');            
        });

        DB::unprepared(
            /** lang Mysql */
            '
            CREATE TRIGGER `reserva_BINS` 
            BEFORE INSERT ON `reserva` 
            FOR EACH ROW
            BEGIN
                IF NEW.data_final_reserva IS NULL THEN
                        SET NEW.data_final_reserva = DATE_ADD(NOW(), INTERVAL 3 DAY);
                END IF;
            END'      
        );
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserva');
    }
}
