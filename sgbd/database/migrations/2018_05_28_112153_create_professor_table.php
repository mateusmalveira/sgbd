<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professor', function (Blueprint $table) {
            $table->integer('matricula_professor')->primary();
            $table->enum('regime_trabalho', ['20h','40h','DE']);
            $table->string('curso_cod_curso',3);
            $table->integer('id_user')->unsigned();
            $table->foreign('curso_cod_curso')->references('cod_curso')->on('curso')->onDelete('cascade')->onUpdate('cascade');           
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professor');
    }
}
