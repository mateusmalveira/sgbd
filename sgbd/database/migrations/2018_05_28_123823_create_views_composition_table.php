<?php

use Illuminate\Database\Migrations\Migration;

class CreateViewsCompositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //View
        DB::unprepared(
            /** @lang MySQL */
            '
            CREATE OR REPLACE  VIEW `aluno_curso` AS
            SELECT 
                `a`.`matricula_aluno` AS `Matricula`,
                `u`.`name` AS `Nome`,
                `c`.`nome_curso` AS `Curso`,
                `u`.`username` AS `Usuario`,
                `u`.`telefone` AS `Telefone`,
                GROUP_CONCAT(
                    `u`.`endereço`,\' \',
                    `u`.`bairro`,\' \',
                    `u`.`cidade`,\'-\',
                    `u`.`estado`
                SEPARATOR \',\') AS `Endereço`,
                `a`.`data_ingreso` AS `Ingresso`,
                `a`.`data_conclusao_prevista` AS `DataConclusao`
            FROM
                (`users` `u`
                    JOIN (`aluno` `a`
                    JOIN `curso` `c` ON ((`a`.`curso_cod_curso` = `c`.`cod_curso`))) ON ((`a`.`id_user` = `u`.`id`)))
                    GROUP BY `a`.`matricula_aluno`
                    ORDER BY `u`.`name` , `c`.`nome_curso`'
            );

            //Funcionarios
            DB::unprepared(
                /** @lang MySQL */
                '
                CREATE OR REPLACE  VIEW `funcionarios` AS
                    SELECT 
                       `f`.`matricula_funcionario` AS `Siape`,
                       `u`.`name` AS `Nome`,
                       `u`.`cpf` AS `CPF`,
                       `c`.`nome_curso` AS `Curso`,
                       `u`.`username` AS `Usuario`,
                       `u`.`telefone` AS `Telefone`,
                       GROUP_CONCAT(
                           `u`.`endereço`,\' \',
                           `u`.`bairro`,\' \',
                           `u`.`cidade`,\'-\',
                           `u`.`estado`
                        SEPARATOR \',\') AS `Endereço`
                    FROM
                        (`users` `u`
                            JOIN (`funcionario` `f`
                            JOIN `curso` `c` ON ((`c`.`cod_curso` = `f`.`cod_curso`))) ON ((`f`.`id_user` = `u`.`id`)))
                            GROUP BY `f`.`matricula_funcionario`
                            ORDER BY `u`.`name`
            ');

            //View Professores
            DB::unprepared(
                /** @lang MySQL */
                '
                CREATE OR REPLACE VIEW `professores` AS                
                SELECT 
                    `U`.`name` AS `Nome`,
                    `U`.`username` AS `Username`,
                    `P`.`matricula_professor` AS `Siape`,
                    `U`.`cpf` AS `CPF`,
                    GROUP_CONCAT(
                        `U`.`endereço`,\' \',
                        `U`.`bairro`,\' \',
                        `U`.`cidade`,\'-\',
                        `U`.`estado`
                     SEPARATOR \',\') AS `Endereço`,
                    `P`.`regime_trabalho` AS `Regime`,
                    `U`.`telefone` AS `Telefone`,
                    `C`.`nome_curso` AS `curso`
                FROM
                    (`users` `U`
                    JOIN (`curso` `C`
                    JOIN `professor` `P` ON ((`C`.`cod_curso` = `P`.`curso_cod_curso`))) ON ((`P`.`id_user` = `U`.`id`)))
                GROUP BY `P`.`matricula_professor`
                ORDER BY `C`.`nome_curso` , `U`.`name`
            ');
            DB::unprepared(
                /** @lang MySQL */
                '
                CREATE OR REPLACE VIEW  `livros` AS
                SELECT 
                    `l`.`ISBN` AS `ISBN`,
                    `l`.`titulo` AS `titulo`,
                    `l`.`ano_lancamento` AS `ano_lancamento`,
                    `l`.`editora` AS `editora`,
                    `l`.`quantidade_copias` AS `total`,
                    `l`.`disponiveis` AS `disponiveis`,
                    `c`.`nome` AS `categoria`,
                    GROUP_CONCAT(`a`.`nome`
                        SEPARATOR \',\') AS `autor`
                FROM
                    ((`livro` `l`
                    JOIN `categoria` `c` ON ((`c`.`id_categoria` = `l`.`id_categoria`)))
                    JOIN (`livro_has_autores` `lr`
                    JOIN `autor` `a` ON ((`lr`.`autores_cpf` = `a`.`cpf`))) ON ((`l`.`ISBN` = `lr`.`livro_ISBN`)))
                GROUP BY `l`.`ISBN`
            ');

            //Reservas
            DB::unprepared(
                /** @lang MySQL */
                '
                CREATE OR REPLACE VIEW `livro_reserva` AS
                    SELECT DISTINCT
                        `E`.`ISBN` AS `ISBN`,
                        `L`.`titulo` AS `titulo_livro`,
                        `U`.`name` AS `Nome`,
                        `U`.`username` AS `Username`,                      
                        `E`.`data_reserva` AS `data_reserva`,
                        `E`.`data_final_reserva` AS `data_final_reserva`
                    FROM
                        (`users` `U`
                        JOIN (`reserva` `E`
                        JOIN `livro` `L` ON ((`L`.`ISBN` = `E`.`ISBN`))) ON ((`E`.`id_user` = `U`.`id`)))
                    ORDER BY `U`.`name` , `L`.`titulo`          
            ');
            //Emprestimos
            DB::unprepared(
                /** @lang MySQL */
                '
                CREATE OR REPLACE VIEW `emprestimos` AS
                    SELECT DISTINCT
                        `U`.`name` AS `Nome`,
                        `U`.`username` AS `username`,
                        `U`.`tipo_usuario` AS `Tipo`,
                        `L`.`titulo` AS `titulo_livro`,
                        `E`.`data_emprestimo` AS `data_emprestimo`,
                        `E`.`data_devolucao` AS `data_devolucao`,
                        `E`.`ISBN` AS `ISBN`,
                        `E`.`id` AS `NE` 
                    FROM
                        (`users` `U`
                        JOIN (`emprestimo` `E`
                        JOIN `livro` `L` ON ((`L`.`ISBN` = `E`.`ISBN`))) ON ((`E`.`id_user` = `U`.`id`)))
                    ORDER BY `U`.`name` , `L`.`titulo`                    
                ');
                
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(
            /** @lang MySQL */
                'DROP VIEW if exists aluno_curso;'
            );
        DB::unprepared(
            /** @lang MySQL */
                'DROP VIEW if exists funcionarios;'
            );
        DB::unprepared(
            /** @lang MySQL */
                'DROP VIEW if exists professores;'
        );
        DB::unprepared(
            /** @lang MySQL */
                'DROP VIEW if exists livros;'
        );
        DB::unprepared(
            /** @lang MySQL */
                'DROP VIEW if exists livro_reserva;'
        );
        DB::unprepared(
            /** @lang MySQL */
                'DROP VIEW if exists emprestimos;'
        );
        
    }
}
