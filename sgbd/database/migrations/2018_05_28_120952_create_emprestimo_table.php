<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmprestimoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprestimo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ISBN',45);    
            $table->integer('id_user')->unsigned();                   
            $table->timestamp('data_emprestimo')->nullable(false)->useCurrent();
            $table->dateTime('data_devolucao')->nullable(true);
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ISBN')->references('ISBN')->on('livro')->onDelete('cascade')->onUpdate('cascade');   
        });
        //Triggers
        DB::unprepared(
            /** lang Mysql */
            '
            CREATE TRIGGER `emprestimo_BINS` 
            BEFORE INSERT ON `emprestimo` 
            FOR EACH ROW
            BEGIN
                    set @quantidade_copias = (select quantidade_copias from livro where ISBN = new.ISBN);
                    set @quantidade_copias_pedidas = (select count(ISBN) from emprestimo where ISBN = new.ISBN);
                    set @copias_restantes = @quantidade_copias - @quantidade_copias_pedidas;    
                    set @copias_pedidas_usuario = (select count(id_user) from emprestimo where id_user = new.id_user);  
                    set @termino_curso_aluno = (select data_conclusao_prevista from aluno where id_user = new.id_user);   
                    set @tipo_usuario = (select tipo_usuario from users where id = new.id_user);               
                    if @copias_restantes then
                        if @tipo_usuario <=> \'aluno\' and @copias_pedidas_usuario < 3 then
                            if @termino_curso_aluno > NOW() then
                                set new.data_devolucao = date_add(now(), interval 15 day);                                
                            else
                                SIGNAL sqlstate \'45001\' set message_text = "O aluno já acabou o curso.";
                            end if;
                        elseif @tipo_usuario <=> \'funcionario\' and @copias_pedidas_usuario < 4 then 
			                set new.data_devolucao = date_add(now(), interval 21 day);			
		                elseif @tipo_usuario <=> \'professor\' and @copias_pedidas_usuario < 5 then 
			                set new.data_devolucao = date_add(now(), interval 30 day);			
                        else
                            SIGNAL sqlstate \'45001\' set message_text = "Não pode-se emprestar mais livros";
                        end if;
                    else
                        SIGNAL sqlstate \'45001\' set message_text = "Não há mais cópias disponíveis";
                    end if;
                END '
            );
            
            DB::unprepared(
                /** lang Mysql */
                '
                CREATE TRIGGER `emprestimo_AINS`
                AFTER INSERT ON `emprestimo` 
                FOR EACH ROW
                begin
                    set @quantidade_copias = (select quantidade_copias from livro where ISBN = new.ISBN);
                    set @quantidade_copias_pedidas = (select count(ISBN) from emprestimo where ISBN = new.ISBN);
                    set @copias_restantes = @quantidade_copias - @quantidade_copias_pedidas;  
                    update livro set disponiveis=@copias_restantes where ISBN=new.ISBN;
                end'            
            );

            DB::unprepared(
                /** lang Mysql */
                '
                CREATE TRIGGER `emprestimo_ADEL` AFTER DELETE ON `emprestimo` 
                FOR EACH ROW
                begin
                    set @ISBN = old.ISBN;
                    set @quantidade_copias = (select disponiveis from livro where ISBN = old.ISBN);
                    set @quantidade_copias_pedidas = (select count(ISBN) from emprestimo where ISBN = old.ISBN AND id_user=old.id_user );
                    set @copias_restantes = @quantidade_copias + @quantidade_copias_pedidas;
                    update livro set disponiveis=@copias_restantes where ISBN=@ISBN;
                END'            
            );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emprestimo');
    }
}
