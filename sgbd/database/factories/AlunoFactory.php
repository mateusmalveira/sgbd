<?php

use Faker\Generator as Faker;
use sgbd\Aluno;

$factory->define(Aluno::class, function (Faker $faker) {
    return [
        'matricula_aluno'=>$faker->unique()->numberBetween(10**4,10**5-1),
        'curso_cod_curso'=>$faker->randomElement($array = ['CEC','CEE','CPS','CCE','CFC','CMC','CMD']),
        'data_ingreso' => $faker->dateTime(), 
    ];
});
