<?php

use Illuminate\Database\Seeder;
use sgbd\Autores;

class AutoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Autores::insert([

            [
                'nome' =>'Paulo Vieira',
                'nacionalidade' => 'Brasileiro',
            ],
            [
                'nome' => 'Charles Duhigg',
                'nacionalidade' => 'Estadunidense',
            ],
            [
                'nome' => 'Stephen Hawking',
                'nacionalidade' => 'Inglês',
            ],
            [
                'nome' =>  'Neil Degrasse Tyson',
                'nacionalidade' =>'Estadunidense',
            ],
            [
                'nome' =>'James Stewart',
                'nacionalidade' => 'Estadunidense',
            ],          
            [
                'nome' =>  'Theodore Gray',
                'nacionalidade' => 'Estadunidense',
            ],
            [
                'nome' => 'Roger S. Pressman',
                'nacionalidade' =>  'Estadunidense',
            ],
            [
                'nome' => 'Ian Sommerville',
                'nacionalidade' => 'Inglês',
            ],
            [
                'nome' =>  'Frederic Skinner',
                'nacionalidade' =>  'Estadunidense',
            ],
            [
                'nome' =>  'Alexandre Sartoris',
                'nacionalidade' =>  'Brasileiro',
            ],  
            [
                'nome' => 'Machado de Assis',
                'nacionalidade' => 'Brasileiro',
            ],   
            [
                'nome' => 'Marcos Debritto',
                'nacionalidade' => 'Brasileiro',
            ],   
            [
                'nome' =>  'Rô Mierling',
                'nacionalidade' => 'Brasileira',
            ],   
            [
                'nome' =>  'Luís Augusto Fischer',
                'nacionalidade' => 'Brasileiro',
            ],   
            [
                'nome' =>  'Felipe Castilho',
                'nacionalidade' =>  'Brasileiro',
            ],     
            [
                'nome' =>   'Anne Frank', 
                'nacionalidade' => 'Alemã',
            ],  
            [
                'nome' => 'Eva Schloss',
                'nacionalidade' => 'Austríaca' ,
            ],
            [
                'nome' => 'Douglas Adams',  
                'nacionalidade' => 'Inglês', 
            ],
            [
                'nome' =>  'Dan Brown',
                'nacionalidade' => 'Estadunidense',  
            ],
            [
                'nome' =>  'Neil Gaiman',
                'nacionalidade' =>  'Inglês',
            ],
            [
                'nome' =>   'Amanda Palmer',
                'nacionalidade' =>   'Estadunidense', 
            ],
            [
                'nome' =>   'Richard Mayer',
                'nacionalidade' =>   'Estadunidense',
            ],
            [
                'nome' =>  'Edgar Allan Poe',
                'nacionalidade' =>  'Estadunidense', 
            ],
            [
                'nome' =>   'André Vianco', 
                'nacionalidade' => 'Brasileiro', 
            ],
            
            [
                'nome' => 'Eduardo Spohr',
                'nacionalidade' =>  'Brasileiro',  
            ],
            [
                'nome' =>  'J. R. R. Tolkien',
                'nacionalidade' => 'Inglês', 
            ],
            [
                'nome' =>  'George Martin',
                'nacionalidade' => 'Estadunidense',
            ],
            [
                'nome' =>  'Stephen King',
                'nacionalidade' => 'Estadunidense',
            ],
            [
                'nome' => 'H. P. Lovecraft',
                'nacionalidade' => 'Estadunidense',
            ],
            [
                'nome' =>  'Agatha Christie',
                'nacionalidade' => 'Inglesa',
            ]             
        
        ]);
    }
}
