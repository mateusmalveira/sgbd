<?php

use Illuminate\Database\Seeder;
use sgbd\Livros;
use sgbd\Categorias;
use sgbd\Autores;
use Illuminate\Support\Facades\DB;

class LivrosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Livros::insert([
            [
                'ISBN' => '978-8580414059',
                'titulo' => 'Fortaleza Digital' ,
                'ano_lancamento' => '2015' ,
                'editora' => 'Arqueiro' ,
                'quantidade_copias' => rand(1, 20),
                'id_categoria' => Categorias::select('id_categoria')->where('nome','Aventura')->value('id_categoria'),
            ],
            
            [
                'ISBN' => '978-8580415544',
                'titulo' => 'O Guia Definitivo do Mochileiro Das Galáxias' ,
                'ano_lancamento' => '2016' ,
                'editora' => 'Arqueiro' ,
                'quantidade_copias' => rand(1, 20),
                'id_categoria' => Categorias::select('id_categoria')->where('nome','Aventura')->value('id_categoria'),
            ],
            [
                'ISBN' => '978-8579305399',
                'titulo' => 'Depois de Auschwitz - o Emocionante Relato de Uma Jovem Que Sobreviveu ao Holocausto' ,
                'ano_lancamento' => '2013' ,
                'editora' => 'Universo Dos Livros' ,
                'quantidade_copias' => rand(1, 20),
                'id_categoria' => Categorias::select('id_categoria')->where('nome','Aventura')->value('id_categoria'),
            ],
            [
                'ISBN' => '978-8502197480',
                'titulo' => 'Estatística e Introdução À Econometria' ,
                'ano_lancamento' => '2013' ,
                'editora' => 'Saraiva' ,
                'quantidade_copias' => rand(1, 20),
                'id_categoria' => Categorias::select('id_categoria')->where('nome','Aventura')->value('id_categoria'),
            ],
            [
                'ISBN' => '978-8595810099',
                'titulo' => 'A Casa Dos Pesadelos' ,
                'ano_lancamento' => '2018' ,
                'editora' => 'Faro Editorial' ,
                'quantidade_copias' => rand(1, 20),
                'id_categoria' => Categorias::select('id_categoria')->where('nome','Aventura')->value('id_categoria'),
            ],
            [
                'ISBN' => '978-8576860761',
                'titulo' => 'A Batalha do Apocalipse' ,
                'ano_lancamento' => '2010' ,
                'editora' => 'Verus' ,
                'quantidade_copias' => rand(1, 20),
                'id_categoria' => Categorias::select('id_categoria')->where('nome','Aventura')->value('id_categoria'),
            ]

        ]);
        
        DB::table('livro_has_autores')->insert([
            [
                'livro_ISBN' => '978-8580414059',
                'autores_cpf' => Autores::select('cpf')->where('nome','Dan Brown')->value('cpf'),   
            ],
            [
                'livro_ISBN' => '978-8580415544',
                'autores_cpf' => Autores::select('cpf')->where('nome','Douglas Adams')->value('cpf'), 
            ],
            [
                'livro_ISBN' => '978-8579305399',
                'autores_cpf' => Autores::select('cpf')->where('nome','Eva Schloss')->value('cpf'), 
            ],
            [
                'livro_ISBN' => '978-8502197480',
                'autores_cpf' => Autores::select('cpf')->where('nome','Alexandre Sartoris')->value('cpf'), 
            ],
            [
                'livro_ISBN' => '978-8595810099',
                'autores_cpf' => Autores::select('cpf')->where('nome','Marcos Debritto')->value('cpf'), 
            ],
            [
                'livro_ISBN' => '978-8576860761',
                'autores_cpf' => Autores::select('cpf')->where('nome','Eduardo Spohr')->value('cpf'), 
            ]

        ]);

    }
}
