<?php

use Illuminate\Database\Seeder;
use sgbd\Admin;
use Illuminate\Support\Facades\Hash;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::insert([
            [
                'email' => 'mateusmalveira@alu.ufc.br',
                'name' => 'Mateus Malveira',
                'username' =>'mateusmalveira',
                'password' => Hash::make('password'),
            ]
        ]);
    }    
}
