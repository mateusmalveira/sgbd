<?php

namespace sgbd;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    protected $table='categoria';
    protected $primaryKey='id_categoria'; 

    public $timestamps=false;

    protected $fillable=[        
        'descricao',
        'nome', 
        'id_categoria'    
    ];

    protected $guarded=[

    ];

    public function livro()
    {
        return $this->hasOne(Livros::class,'id_categoria');
    }
}
