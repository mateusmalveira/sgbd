<?php

namespace sgbd;

use Illuminate\Database\Eloquent\Model;

class Livros extends Model
{
    public $incrementing = false;
    protected $table='livro';
    protected $primaryKey='ISBN'; 

    public $timestamps=false;

    protected $fillable=[
        'ISBN',
        'titulo',
        'ano_lancamento',
        'editora',
        'quantidade_copias',
        'id_categoria',
        'disponiveis'        
    ];

    protected $guarded=[

    ];
}
