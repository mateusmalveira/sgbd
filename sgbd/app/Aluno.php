<?php

namespace sgbd;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    //Set Table Aluno e Primary Key
    protected $table='aluno';
    protected $primaryKey='matricula_aluno';     

    public $timestamps=false;
    //Arquivos disponíveis para edição
    protected $fillable=[
        'matricula_aluno',
        'curso_cod_curso',
        'data_ingreso',
        'id_user', 
    ];
}
