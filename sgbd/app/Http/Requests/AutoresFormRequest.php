<?php

namespace sgbd\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AutoresFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($input['_method']='PATCH'):
            return [
                'nome' => 'required',
                'nacionalidade' => 'required'

            ];

        elseif($input['_method']='POST'):

            return [
                'cpf' => 'required|unique:autor|min:11|max:11',
                'nome' => 'required',
                'nacionalidade' => 'required'
            ];
        else:

            return [
                'cpf' => 'required|unique:autor|min:11|max:11',
                'nome' => 'required',
                'nacionalidade' => 'required'
            ];

        endif;

        
        
    }
}
     