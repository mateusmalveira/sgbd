<?php

namespace sgbd\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;
use Auth;
use sgbd\User;
use sgbd\Aluno;
use sgbd\Professor;
use sgbd\Funcionarios;
use sgbd\Cursos;
use sgbd\Biblio;

class EditCadUserController extends Controller
{
    public function editAluno($id){
        //check $id = $iduser
        if($id != (Auth::user()->id)):
            return Redirect::to('/home')            
            ->withErrors('Gaiato né você? Você não pode editar dados do amiguinho');
        endif;
                        
        //verifica se o usuario é aluno mesmo
        if(Auth::user()->tipo_usuario != 'aluno' ):
            return Redirect::to('/home')->withErrors('Você não pode editar dados de outro tipo de usuario');
        endif;
        //busca os dados do aluno

        $user = User::findOrFail($id);   
        $matricula = Aluno::select('matricula_aluno')->where('id_user',$id)->first();
        $cursocod = Aluno::select('curso_cod_curso')->where('id_user',$id)->value('curso_cod_curso');
        $curso = Cursos::select('nome_curso')->where('cod_curso',$cursocod)->first();          
        return view("usuarios.alunos.perfil",["user"=>User::findOrFail($id),'matricula'=>$matricula,'curso'=>$curso]);  
        
    }
    public function saveAluno(Request $request, $id){
        //Verifica se contém erros
        $validator = Validator::make($request->all(), [            
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|'.Rule::unique('users')->ignore($id),
            'email' => 'required|string|email|max:255|'.Rule::unique('users')->ignore($id),                      
        ]); 
        if ($validator->fails()) { 
            return redirect('edit/6o4sDQ5YzG89HQJvggHa/'.$id.'/profile') 
            ->withErrors($validator)
            ->withInput();
         } 
         //Caso não
        $user = User::findOrFail($id);
        $user->email = $request->get('email');
        $user->endereço = $request->get('logradouro'); 
        $user->cep = $request->get('cep');
        $user->bairro = $request->get('bairro');
        $user->cidade = $request->get('cidade');
        $user->estado = $request->get('estado');
        $user->telefone = $request->get('telefone');
        $user->username = $request->get('username'); 
        if($request->get('password') != ''){
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:6|confirmed',
            ]);
            if ($validator->fails()) {           
                return redirect('edit/6o4sDQ5YzG89HQJvggHa/'.$id.'/profile')    
                            ->withErrors($validator)
                            ->withInput();                     
            }
        $user->password = Hash::make($request['password']);
        $user->update();
        return Redirect::to('/home')->with('success','Seu cadastro foi alterado com sucesso'); 
        }
        $user->update();
        return Redirect::to('/home')->with('success','Seu cadastro foi alterado com sucesso'); 
    }

    //Professor
    public function editProfessor($id){
        //check $id = $iduser
        if($id != (Auth::user()->id)):
            return Redirect::to('/home')            
            ->withErrors('Você só pode editar o usuário que pertence a você');
        endif;
                        
        //verifica se o usuario é aluno mesmo
        if(Auth::user()->tipo_usuario != 'professor' ):
            return Redirect::to('/home')->withErrors('Você só pode editar o usuário que pertence a você');
        endif;
        //busca os dados do aluno

        $user = User::findOrFail($id);
        $regime = Professor::select('regime_trabalho')->where('id_user',$id)->first();
        $siape = Professor::select('matricula_professor')->where('id_user',$id)->first();
        $cursocod = Professor::select('curso_cod_curso')->where('id_user',$id)->value('curso_cod_curso');
        $curso = Cursos::select('nome_curso')->where('cod_curso',$cursocod)->first();           
        return view("usuarios.professores.perfil",["user"=>User::findOrFail($id),'professor'=>$regime,'curso'=>$curso,'siape'=>$siape]);  
        
    }

    public function saveProfessor(Request $request, $id){
        //Verifica se contém erros
        $validator = Validator::make($request->all(), [            
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|'.Rule::unique('users')->ignore($id),
            'email' => 'required|string|email|max:255|'.Rule::unique('users')->ignore($id),  
            'regime' => 'string|required',                    
        ]); 
        if ($validator->fails()) { 
            return redirect('edit/Z80SQTOlQRapUuc2dwzN/'.$id.'/profile') 
            ->withErrors($validator)
            ->withInput();
         } 
         //Caso não
        $user = User::findOrFail($id);
        $user->email = $request->get('email');
        $user->endereço = $request->get('logradouro'); 
        $user->cep = $request->get('cep');
        $user->bairro = $request->get('bairro');
        $user->cidade = $request->get('cidade');
        $user->estado = $request->get('estado');
        $user->telefone = $request->get('telefone');
        $user->username = $request->get('username');
        $search = Professor::select('matricula_professor')
        ->where('id_user',$id)->value('matricula_professor');
        $siape = Professor::findOrFail($search);
        $siape->regime_trabalho = $request->get('regime');
        if($request->get('password') != ''){
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:6|confirmed',
            ]);
            if ($validator->fails()) {           
                return redirect('edit/Z80SQTOlQRapUuc2dwzN/'.$id.'/profile')    
                            ->withErrors($validator)
                            ->withInput();                     
            }
        $user->password = Hash::make($request['password']);
        $user->update();
        $siape->update();
        return Redirect::to('/home')->with('success','Seu cadastro foi alterado com sucesso'); 
        }
        $user->update();
        $siape->update();
        return Redirect::to('/home')->with('success','Seu cadastro foi alterado com sucesso'); 
    }
    public function editFuncionario($id){
        //check $id = $iduser
        if($id != (Auth::user()->id)):
            return Redirect::to('/home')            
            ->withErrors('Você não pode editar o perfil de outro usuário');
        endif;
                        
        //verifica se o usuario é aluno mesmo
        if(Auth::user()->tipo_usuario != 'funcionario' ):
            return Redirect::to('/home')->withErrors('Você não pode editar dados de outro tipo de usuario');
        endif;
        //busca os dados do aluno

        $user = User::findOrFail($id);
        $siape = Funcionarios::select('matricula_funcionario')->where('id_user',$id)->first();
        $cursocod = Funcionarios::select('cod_curso')->where('id_user',$id)->value('cod_curso');
        $curso = Cursos::select('nome_curso')->where('cod_curso',$cursocod)->first(); 
        return view("usuarios.funcionarios.perfil",["user"=>User::findOrFail($id),'curso'=>$curso,'siape'=>$siape]);        
    }
    public function saveFuncionario(Request $request, $id){
        //Verifica se contém erros
        $validator = Validator::make($request->all(), [            
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|'.Rule::unique('users')->ignore($id),
            'email' => 'required|string|email|max:255|'.Rule::unique('users')->ignore($id),                      
        ]); 
        if ($validator->fails()) { 
            return redirect('edit/m6mOMxQZVCE0UQVSgfwI/'.$id.'/profile') 
            ->withErrors($validator)
            ->withInput();
         } 
         //Caso não
        $user = User::findOrFail($id);
        $user->email = $request->get('email');
        $user->endereço = $request->get('logradouro'); 
        $user->cep = $request->get('cep');
        $user->bairro = $request->get('bairro');
        $user->cidade = $request->get('cidade');
        $user->estado = $request->get('estado');
        $user->telefone = $request->get('telefone');
        $user->username = $request->get('username'); 
        if($request->get('password') != ''){
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:6|confirmed',
            ]);
            if ($validator->fails()) {           
                return redirect('edit/m6mOMxQZVCE0UQVSgfwI/'.$id.'/profile')    
                            ->withErrors($validator)
                            ->withInput();                     
            }
        $user->password = Hash::make($request['password']);
        $user->update();
        return Redirect::to('/home')->with('success','Seu cadastro foi alterado com sucesso'); 
        }
        $user->update();
        return Redirect::to('/home')->with('success','Seu cadastro foi alterado com sucesso'); 
    }
    //Bibliotecario
    public function editBiblio($id){
        //check $id = $iduser
        if($id != (Auth::user()->id)):
            return Redirect::to('/biblio')            
            ->withErrors('Voce só pode editar o seu perfil');
        endif;

        $user = Biblio::findOrFail($id);  
        return view("usuarios.bibliotecarios.perfil",["user"=>$user]);  
        
    }
    public function saveBiblio(Request $request, $id){
        //Verifica se contém erros
        $validator = Validator::make($request->all(), [ 
            'username' => 'required|string|max:20|'.Rule::unique('biblios')->ignore($id),
            'email' => 'required|string|email|max:255|'.Rule::unique('biblios')->ignore($id),                      
        ]); 
        if ($validator->fails()) { 
            return redirect('biblio/edit/'.$id.'/profile') 
            ->withErrors($validator)
            ->withInput();
         } 
         //Caso não
        $user = Biblio::findOrFail($id);
        $user->email = $request->get('email');       
        $user->username = $request->get('username'); 
        if($request->get('password') != ''){
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:6|confirmed',
            ]);
            if ($validator->fails()) {           
                return redirect('biblio/edit/'.$id.'/profile')    
                            ->withErrors($validator)
                            ->withInput();                     
            }
        $user->password = Hash::make($request['password']);
        $user->update();
        return Redirect::route('biblio.dashboard')->with('success','Seu cadastro foi alterado com sucesso'); 
        }
        $user->update();
        return Redirect::route('biblio.dashboard')->with('success','Seu cadastro foi alterado com sucesso'); 
    }

}
