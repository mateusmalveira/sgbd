<?php

namespace sgbd\Http\Controllers;

use Illuminate\Http\Request;
use sgbd\Http\Requests;
use sgbd\Cursos;
use Illuminate\Support\Facades\Redirect;
use sgbd\Http\Requests\CursosFormRequest;
use DB;
class CursosController extends Controller
{
    public function __construct(){

      //  $this->middleware('auth');
    }

    public function index(Request $request){

        if ($request){
            $query=trim($request->get('searchText'));
            $cursos=DB::table('curso')
                ->where('nome_curso','LIKE','%'.$query.'%')
                ->orderBy('nome_curso','asc')
                ->orwhere('cod_curso','LIKE','%'.$query.'%')
                ->orderBy('nome_curso','asc')
                ->paginate(10);
            return view('cursos.index',["cursos"=>$cursos,"searchText"=>$query ]);
        }
    }

    public function  create(){
        return view("cursos.create");
    }
  
    public function store( CursosFormRequest $request){
        //ele armazena na banco de dados
        $cursos=new Cursos;
        $cursos->nome_curso=trim(mb_strtoupper($request->get('nome_curso'),'UTF-8'));
        $cursos->cod_curso=trim(mb_strtoupper($request->get('cod_curso'),'UTF-8'));
        $cursos->save();
        return Redirect::to('/admin/cursos')->with('success','Curso adicionado com sucesso!!');
    }
    //show
    public function show($id){
        return view("cursos.show",["cursos"=>Cursos::findOrFail($id)]);
    }
    //edit
    public function edit($id){
        return view("cursos.edit",["cursos"=>Cursos::findOrFail($id)]);       
    }
    //update
    public function update(CursosFormRequest $request,$id){
        $cursos=Cursos::findOrFail($id);
        $cursos->nome_curso=trim(mb_strtoupper($request->get('nome_curso'),'UTF-8'));
        $cursos->update();
        return Redirect::to('/admin/cursos')->with('success','Curso atualizado com sucesso');
    }
    //delete 
    public function destroy($id){
        $cursos=Cursos::findOrFail($id);
        $cursos->delete();
        return Redirect::to('/admin/cursos')->with('success','Curso deletado com sucesso');
    }
}
