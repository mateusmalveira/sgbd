<?php

namespace sgbd\Http\Controllers\Auth;

use sgbd\User;
use sgbd\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */    
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'cpf' => 'required|string|unique:users|max:11|min:11',
            'tipo'=> 'required|string',
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'regime' => 'string',
            'curso' => 'required|string',
            'matricula' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \sgbd\User
     */
    protected function create(array $data)
    {     
        $user = User::create([
            'name' => $data['name'],
            'cpf' =>$data['cpf'],
            'endereço' =>$data['logradouro'],
            'cep' =>$data['cep'],
            'bairro' =>$data['bairro'],
            'cidade' =>$data['cidade'],
            'estado' =>$data['estado'],
            'telefone'=>$data['telefone'], 
            'username'=>$data['username'], 
            'tipo_usuario'=>$data['tipo'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $userId = $user->id;
        if($data['tipo'] == 'funcionario'):
            DB::table('funcionario')->insert([
                'matricula_funcionario'=>$data['matricula'],
                'id_user'=>$userId,
                'cod_curso'=>$data['curso'],
                ]);
            return $user;

        elseif($data['tipo'] == 'aluno'):
            DB::table('aluno')->insert([
                'matricula_aluno'=>$data['matricula'],
                'id_user'=>$userId,                
                'curso_cod_curso'=>$data['curso'],  
                ]);
            return $user;

        elseif($data['tipo'] == 'professor'):
            DB::table('professor')->insert([
                'matricula_professor'=>$data['matricula'],
                'id_user'=>$userId,               
                'curso_cod_curso'=>$data['curso'], 
                'regime_trabalho'=>$data['regime'], 
                ]);
            return $user;
        else:
            return false;
        endif;    
    }
}
