<?php

namespace sgbd;

use Illuminate\Database\Eloquent\Model;

class Autores extends Model
{
    protected $table='autor';
    protected $primaryKey='cpf'; 

    public $timestamps=false;

    protected $fillable=[
        'cpf',
        'nome',
        'nacionalidade'     
    ];

    protected $guarded=[

    ];
}
